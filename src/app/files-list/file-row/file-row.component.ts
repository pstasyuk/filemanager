import {Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { File } from '../file'

@Component({
  selector: '[app-file-row]',
  templateUrl: './file-row.component.html',
  styleUrls: ['./file-row.component.css']
})
export class FileRowComponent implements OnInit {

  @Input()
  item: File;

  @Output()
  onFolderSelected = new EventEmitter<File>();

  constructor() { }

  ngOnInit() {
  }

  onSelectFolder(folder: File) {
    this.onFolderSelected.emit(folder);
  }

}
