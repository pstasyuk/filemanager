import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import {SharedModule} from "./shared/shared.module";

import { AppComponent } from './app.component';
import { FilesListComponent } from './files-list/files-list.component';
import { AppRoutingModule } from './/app-routing.module';
import { FileRowComponent } from './files-list/file-row/file-row.component';
import { HeaderComponent } from './header/header.component';

import { FileService } from "./files-list/file.service";


@NgModule({
  declarations: [
    AppComponent,
    FilesListComponent,
    FileRowComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [FileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
