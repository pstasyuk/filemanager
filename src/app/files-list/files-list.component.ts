import { Component, OnInit } from '@angular/core';
import { FileService } from "./file.service";
import { File} from "./file";

@Component({
  selector: 'app-files-list',
  templateUrl: './files-list.component.html',
  styleUrls: ['./files-list.component.css']
})
export class FilesListComponent implements OnInit {

  currentFolder: File;
  foldersStack: File[] = [];
  error: any;

  constructor(private fileService: FileService) { }

  ngOnInit() {
    this.fileService.getTree().subscribe(tree => {
      this.currentFolder = tree;
      this.foldersStack.push(tree)
    }, err => {
      this.error = err;
    })
  }

  onFolderSelected(folder: File): void {
    this.currentFolder = folder;
    this.foldersStack.push(folder);
  }

  onBack(): void {
    this.foldersStack.splice(-1,1);
    this.currentFolder =  this.foldersStack[this.foldersStack.length - 1];
  }

  buildFullPath() {
    return '/' + this.foldersStack.map(file => file.name).join('/')
  }

}
