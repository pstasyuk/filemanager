export class File {
  name: string;
  isFolder: boolean;
  size: number;
  date: number;
  items: [File]
}
