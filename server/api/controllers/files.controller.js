const config = require("./../../config/vars");
const FileTreeService = require("./../services/fileTree.service");

exports.list = function (req, res, next) {

  let builder = new FileTreeService(config.workFolder);
  builder.buildTree().then(tree => {
    res.status(200).json(tree)
  }).catch(err => next(err));

};
