import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { File } from './file';

@Injectable()
export class FileService {

  constructor(private http: HttpClient) {
  }

  getTree (): Observable<File> {
    return this.http.get<File>(environment.apiServer + '/api/files')
  }

}
