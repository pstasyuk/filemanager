const app = require('./config/express');
const config = require('./config/vars');

app.listen(config.port, () => console.info(`server started on port ${config.port}`));
