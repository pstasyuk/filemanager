const express = require('express');
const controller = require('./../controllers/files.controller');
const router = express.Router();

router.get("/", controller.list);

module.exports = router;
