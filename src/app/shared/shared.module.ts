import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileSizePipe } from './pipes/file-size.pipe';
import { DurationPipe } from './pipes/duration.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FileSizePipe, DurationPipe],
  exports: [FileSizePipe, DurationPipe]
})
export class SharedModule { }
