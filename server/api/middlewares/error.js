exports.serverError = (err, req, res, next) => {
  const response = {
    code: 500,
    message: "Server error",
    errors: err.errors,
    stack: err.stack,
  };

  res.status(500);
  res.json(response);
  res.end();
};

exports.notFound = (req, res, next) => {
  const response = {
    code: 404,
    message: "Not found",
  };

  res.status(404);
  res.json(response);
  res.end();
};
