const fs = require("fs");
const async = require("async");
const path = require("path");

class FileTree {

  constructor(workDir) {
    this.workDir = workDir;
  }

  buildTree() {
    return new Promise((resolve, reject) => {
      fs.stat(this.workDir, (err, stat) => {
        if (err) {
          return reject(Error(err));
        }
        let tree = {
          name: path.basename(this.workDir),
          isFolder: stat.isDirectory(),
          size: stat.size,
          date: stat.birthtime.getTime(),
          items: []
        };
        this._getItemsRecursively(this.workDir, function (err, items) {
          if (err) {
            return reject(Error(err));
          }
          tree.items = items;
          resolve(tree);
        })
      });
    });
  }

  _getItemsRecursively(dir, next) {
    fs.readdir(dir, (err, list) => {
      if (err) {
        return next(err);
      }
      async.map(list, (file, mapCb) => {
        let filePath = path.join(dir, file);
        fs.stat(filePath, (err, stat) => {
          if (err) {
            return mapCb(err);
          }
          let item = {
            name: file,
            isFolder: stat.isDirectory(),
            size: stat.size,
            date: stat.birthtime.getTime(),
            items: []
          };
          if (item.isFolder) {
            this._getItemsRecursively(filePath, function (err, subItems) {
              if (err) {
                return mapCb(err);
              }
              item.items = subItems;
              mapCb(null, item);
            })
          } else {
            mapCb(null, item);
          }
        });
      }, (err, list) => {
        return next(err, list);
      });
    });
  }

}

module.exports = FileTree;
