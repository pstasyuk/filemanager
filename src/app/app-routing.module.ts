import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FilesListComponent} from "./files-list/files-list.component";

const routes: Routes = [
  { path: '', component: FilesListComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule {}
