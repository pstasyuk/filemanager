# FileManager api server

Tested on Node.js v9.5.0 and npm v5.6.0

## 1. Install dependencies

Run `npm i` in server folder.

## 2. Run server

Run `npm run dev` in server folder.

## Options

- You can set working directory by WORK_FOLDER env variable. Default folder is `server/api`
- You can set server port by NODE_PORT env variable. Default port is `3000`
