const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cors = require('cors')
const routes = require('../api/routes');
const error = require('../api/middlewares/error');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.use(methodOverride());

app.use('/api', routes);

app.use(error.serverError);
app.use(error.notFound);

module.exports = app;
