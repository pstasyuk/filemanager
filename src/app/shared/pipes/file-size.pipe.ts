import { Pipe, PipeTransform } from '@angular/core';
import { humanize } from 'humanize';

@Pipe({
  name: 'fileSize'
})
export class FileSizePipe implements PipeTransform {

  transform(value: number, args?: any): string {
    return humanize.filesize(value);
  }

}
