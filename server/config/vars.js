const path = require("path")

module.exports = {
  port: process.env.NODE_PORT || 3000,
  workFolder: process.env.WORK_FOLDER || path.join(__dirname, "..", "api")
};
